'use strict';
const router = require('koa-router')();
const Users = require('../models/users');
router.get('/index', async function(ctx, next) {
    let users = await Users.find({});
    ctx.success(200, '成功', users)
});

module.exports = router;
