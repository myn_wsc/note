'use strict';
const Response = require('../domain/response');
module.exports = () => {
    return async function(ctx, next) {
        try {
            await next();
        } catch (error) {
            ctx.status = 500;
            let response = new Response(ctx.status, error.message)
            ctx.body = response;
        }
    }
}