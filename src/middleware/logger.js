'use strict';
const { debugLogger } = require('./log');
module.exports = () => {
    return async function(ctx, next) {
        ctx.logger = debugLogger;
        await next();
    }
}