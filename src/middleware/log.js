const log4js = require('koa-log4');
const log4jsConf = require('../config/log4js');
log4js.configure(log4jsConf);

module.exports = {
    debugLogger: log4js.getLogger('console'),
    logger: log4js.koaLogger(log4js.getLogger('success'), { level: 'info'}),
    errLogger: log4js.getLogger('errors'),
}