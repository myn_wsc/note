'use strict';
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.ObjectId;

const usersSchema = new Schema({
    _id: { type: ObjectId }, // 默认生成，不加也可以
    username: { type: String, required: [true,'username不能为空'] },
})

const users = mongoose.model('testusers', usersSchema);
module.exports = users;
