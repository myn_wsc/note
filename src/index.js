'use strict';
const Koa = require('koa');
const { logger, debugLogger } = require('./middleware/log');
const ctxLogger = require('./middleware/logger');
const config = require('./config/config.local');
const koaLogger = require('koa-logger');
const ErrorHandler = require('./middleware/error');
const mongoConf = require('./config/mongo')
const fs = require('fs');

const app = new Koa();
// 连接数据库
mongoConf.connect();
// 启动ctx下挂载logger中间件
app.use(ctxLogger())
// 添加错误处理中间件
app.use(ErrorHandler())
// 启动日志文件记录
app.use(logger);
app.use(koaLogger())

app.use(async (ctx, next) => {
    ctx.success = (code = 200, msg = '', data = null) => {
        ctx.status = code;
        ctx.body = {
            code,
            msg,
            data
        }
    }
    await next();
})

fs.readdirSync('src/routers').forEach(route => {
    let api = require(`./routers/${route}`);
    app.use(api.routes(), api.allowedMethods());
})

app.listen(config.port);
debugLogger.info(`[${process.env.npm_package_name}] is starting at port ${config.port}`)