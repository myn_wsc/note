'use strict';
const config = require('./config.local');
const mongoose = require('mongoose');

module.exports = {
    connect: () => {
        mongoose.connect(config.mongodb.url, config.mongodb.options);
        let db = mongoose.connection;
        db.on('error', () => {
            console.log('mongodb 连接错误')
        });
        db.once('open', () => {
            console.log('mongodb connect success');
        })
    }
}