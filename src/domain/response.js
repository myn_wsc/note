'use strict';
class Response {
    constructor(code = 200, msg = '', data = null) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
module.exports = Response;
